Pod::Spec.new do |spec|
  spec.name         = 'FSAntiTheftSDK'
  spec.version      = '0.2.0'
  spec.license      = "MIT"
  spec.homepage     = 'https://Raman_Srivastava@bitbucket.org/raman_srivastava/fsantitheftsdk.git'
  spec.authors      = { 'Raman Srivastava' => 'raman@daffodilsw.com' }
  spec.summary      = 'FSAntiTheft SDK Cocoa Pod'
  spec.description  = 'FSAntiTheft SDK Cocoa Pod for Health App'
  spec.source       = { :git => 'https://Raman_Srivastava@bitbucket.org/raman_srivastava/fsantitheftsdk.git', :tag => spec.version }
# uncomment it for pod lib lint
  spec.vendored_frameworks = "FSAntiTheftSDK.xcframework/ios-arm64/FSAntiTheftSDK.framework"
  spec.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
  spec.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
  spec.platform = :ios
  spec.swift_version = "5.0"
  spec.ios.deployment_target  = '12.0'
spec.dependency "SDWebImage", "~> 5.12.3"
spec.dependency "SwiftLint"
spec.dependency "KeychainAccess"
spec.dependency "MBProgressHUD"
spec.dependency "Firebase/Core"
spec.dependency "Firebase/Auth"
end
