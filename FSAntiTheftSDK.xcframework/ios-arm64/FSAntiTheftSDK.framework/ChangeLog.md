##Changelog


All notable changes to this project are documented in this file.


The changes is based on the following types:

* **Added** for new features.
* **Changed** for changes in existing functionality.
* **Deprecated** for soon-to-be removed features.
* **Removed** for now removed features.
* **Fixed** for any bug fixes.
* **Security** in case of vulnerabilities.
* **Unreleased** for future updates


And this project adheres to Semantic Versioning and the versioning is done with the format MAJOR.MINOR (eg:- v1.4) where

* **MAJOR** version: when you make incompatible changes and major features/functionality changes.
* **MINOR** version: when you add functionality with some fixes.


##[v1.0] - 2022-04-16

### **Added**

Basic functionality:

* Registration Module
* OTP
* Push Notification
* Location Trigger
* Alarm Trigger
* Camera


