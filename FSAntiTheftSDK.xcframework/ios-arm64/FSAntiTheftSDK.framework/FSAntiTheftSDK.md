# **FSAntiTheft SDK**

FSAntiTheft Library is an iOS Library which allow user to track their device remotely.
There are feature like 
- Location Tracking
- Remote Alarm
- MugShot


## Compatibility

1. iOS 12+
2. Xcode 13.0+
3. Device orientation - portrait

## SDK Creation

### 1) Manual Creation
No steps are required

### 2) Automatic Creation
To create compiled sdk (**FSAntiTheftSDK.xcframework**) follow the steps below:

1. Go to terminal in root of the project
2. Run below command to archive sdk for iOS devices
 
		xcodebuild archive -workspace FSAntiTheftSDK.xcworkspace -scheme FSAntiTheftSDK -destination "generic/platform=iOS" -archivePath ../output/FSAntiTheftSDK-iOS SKIP_INSTALL=NO BUILD_LIBRARY_FOR_DISTRIBUTION=YES

3. Run below command to archive sdk for iOS Simulator
		
		xcodebuild archive -workspace FSAntiTheftSDK.xcworkspace -scheme FSAntiTheftSDK -destination "generic/platform=iOS Simulator" -archivePath ../output/FSAntiTheftSDK-Sim SKIP_INSTALL=NO BUILD_LIBRARY_FOR_DISTRIBUTION=YES

4. Run below command to create final fat framework FSAntiTheftSDK.xcframework which will be created in the output folder.

		xcodebuild -create-xcframework -framework  ../output/FSAntiTheftSDK-iOS.xcarchive/Products/Library/Frameworks/FSAntiTheftSDK.framework -framework  ../output/FSAntiTheftSDK-Sim.xcarchive/Products/Library/Frameworks/FSAntiTheftSDK.framework -output  ../output/FSAntiTheftSDK.xcframework

## SDK Deployment

 There are two ways to deploy the BulbshareSDK

### 1) Manual Deployment

 
 A) Add the SDK dependencies in the podfile of the client Application
 
 	pod 'SwiftLint'
 	pod 'KeychainAccess'
 	pod 'MBProgressHUD'
 	pod 'Firebase/Core'
 	pod 'Firebase/Auth'
    pod 'IQKeyboardManagerSwift'   
    pod 'Firebase/Analytics'
    pod 'Firebase/Messaging'
 	
	end
	
B) Now Add **FSAntiTheftSDK.xcframework** which is being created in the **output folder** mentioned in the **SDK Creation** step to Application target
 
C) Go to project target and in general tab choose **FSAntiTheftSDK.xcframework** in framework,libraries and embedded contents and change from **Do not embed** to **embed and sign**


### 2) Automatic Deployment

In Progress.


