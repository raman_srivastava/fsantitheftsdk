# FSAntiTheft SDK 

##  Error Handling

**SDKError** throws Error object if any error occurs, you can check protocol method
 
 These are the registered errors list: 
 
	  SDKError.ServerError.unknownError.rawValue 
	  SDKError.ServerError.userDoesNotExist.rawValue 
	  SDKError.ServerError.invalidPassword.rawValue 
	  SDKError.ServerError.invalid_arguments.rawValue 
	  	  
	  
  
##ServerError Errors

|Error Code 							 | Description		|
|---------------------------|-------------------------------|-----------|
|UNKNOWN_ERROR	|unknownError|
|626	|User does not exist|
|658  |Password is invalid	|There is something wrong at Server side|
|616 |invalid arguments|




